package common.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import common.demo.entities.User;
import common.demo.reponsitories.UserReponsitory;

@Controller
@RequestMapping(value="/")
public class UserController {

	@Autowired
	UserReponsitory userReponsitory;
	
	public String showUsers(Model model){
		List<User> users = (List<User>) userReponsitory.findAll(); 
		model.addAttribute("users", users);
		return "index";
	}
	
	@RequestMapping(value = "/user/adduser")
	public String create(Model model) {
		model.addAttribute("user", new User());
        model.addAttribute("msg", "Thêm mới user");
        model.addAttribute("action", "/user/adduser");
        return "newuser";
	}
}
