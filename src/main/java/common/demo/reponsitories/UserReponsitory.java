package common.demo.reponsitories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import common.demo.entities.User;

public interface UserReponsitory extends CrudRepository<User, Integer>{

	List<User> findByUsernameOrName(String username, String name);
}
