package common.demo.reponsitories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import common.demo.entities.Blog;

public interface BlogReponsitory extends CrudRepository<Blog, Integer>{
	
	List<Blog> findByTitleOrBlogType(String title, String blogType);
}
